"""
Views for accessing language preferences
"""
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseBadRequest


from django.views.decorators.csrf import csrf_exempt


from user_api.models import UserPreference
from lang_pref import LANGUAGE_KEY


@login_required
def set_language(request):
    """
    This view is called when the user would like to set a language preference
    """
    user = request.user
    lang_pref = request.POST.get('language', None)

    if lang_pref:
        UserPreference.set_preference(user, LANGUAGE_KEY, lang_pref)
        return HttpResponse('{"success": true}')

    return HttpResponseBadRequest('no language provided')
 

# Added imports for zad_setlang
from django.conf import settings
from django.utils import translation
from django.utils.translation import check_for_language, to_locale, get_language #, LANGUAGE_SESSION_KEY
from django.utils.http import is_safe_url


# @csrf_exempt    
# #@login_required
# def zad_setlang(request):
#     """
#     This view is called when the user would like to set a language preference
#     """
#     # both of the below lines work
#     user = request.user
#     #user = request.user.id

#     lang_pref = request.GET.get('language', 'ar')

#     if lang_pref:
#         translation.activate(lang_pref)
#         #UserPreference.set_preference(user, LANGUAGE_KEY, lang_pref)
#         #return HttpResponseRedirect('/')
#         #return HttpResponse('{"success": true}')
#         return HttpResponse(str(request))


#     return HttpResponseBadRequest('no language provided')


@csrf_exempt
def zad_setlang(request):
    """
    Redirect to a given url while setting the chosen language in the
    session or cookie. The url and the language code need to be
    specified in the request parameters.

    Since this view changes how the user will see the rest of the site, it must
    only be accessed as a POST request. If called as a GET request, it will
    redirect to the page in the request (the 'next' parameter) without changing
    any state.
    """
    #next = request.POST.get('next', request.GET.get('next'))
    next = request.GET.get('next')
    if not is_safe_url(url=next, host=request.get_host()):
        next = request.META.get('HTTP_REFERER')
        if not is_safe_url(url=next, host=request.get_host()):
            next = '/'
    #response = http.HttpResponseRedirect(next)
    response = HttpResponseRedirect(next)
    #if request.method == 'POST':
    if request.method == 'GET':
        lang_code = request.GET.get('language', None)
        if lang_code and check_for_language(lang_code):
            if hasattr(request, 'session'):
                #request.session[LANGUAGE_SESSION_KEY] = lang_code
                request.session['django_language'] = lang_code
                #return HttpResponse(str(request))
            else:
                response.set_cookie(settings.LANGUAGE_COOKIE_NAME, lang_code,
                                    max_age=settings.LANGUAGE_COOKIE_AGE,
                                    path=settings.LANGUAGE_COOKIE_PATH,
                                    domain=settings.LANGUAGE_COOKIE_DOMAIN)
    return response