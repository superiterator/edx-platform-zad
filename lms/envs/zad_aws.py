'''
I add my Specific configurations, features, applications, and settings related to ZadGroup Project
So I don't need to hack the "common" and "aws" settings files
'''


# I don't need to imort common.py as it is imported in aws.py
from .aws import *



CONTACT_EMAIL 		= "super.iterator@gmail.com , awad@zadgroup.net"
COLLABORATE_EMAIL	= "super.iterator@gmail.com , securedome@yahoo.com"


# I add here my applications so I don't have to edit INSTALLED_APPS in common.py settings file
INSTALLED_APPS += (
					#"zad_contact" ,
				)


