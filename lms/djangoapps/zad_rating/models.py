from django.db import models
from django.forms import Form, ModelForm

from djangoratings.fields import AnonymousRatingField, RatingField

class course_rate(models.Model):
    """Course Rating Model"""
    course_id = models.CharField(max_length=200)
    object_id = models.PositiveIntegerField()
    rating = RatingField(range=5, can_change_vote=True)
    
    def __unicode__(self):
        return self.course_id
