from django.conf.urls import patterns, include, url
from django.views.generic import ListView, CreateView
from .models import course_rate
from djangoratings.views import AddRatingFromModel
from django.core.urlresolvers import reverse

from django.views.decorators.csrf import csrf_exempt



urlpatterns = patterns('zad_rating.views',

    url(r'add_rate/$','add_rate'),


    # url(r'^rate/(?P<object_id>[a-zA-Z0-9]+)/(?P<score>\d+)/$', csrf_exempt( AddRatingFromModel() ), {
    #     'app_label': 'zad_rating',
    #     'model': 'course_rate',
    #     'field_name': 'rating',
    # }),    

	url(u'^rate/(?P<course_id>[\u0620-\u06A4\u0661-\u0669_a-zA-Z0-9.\-=%]+)/(?P<score>\d+)/$', 'course_rating'),

)
