# Django imports
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse

# Edx imports
from branding import modulestore
from opaque_keys.edx.keys import CourseKey

# Python standard library imports
import logging 



log = logging.getLogger(__name__)



store = modulestore()

# course = [course for course in store.get_courses() if course.id]


def course_rating(request, course_id, score):


    log.warning("Inside Course Rating view ..")

    log.warning(u"Incoming course identifier .. {}".format(course_id))


    course_id = course_id.replace('=','/')

    key = CourseKey.from_string(course_id)
    course = store.get_course(key)

    var = course._edit_info

    log.warning(u"Course Edit Info data: {}".format(var))

    # Adding the score to the total rating score count
    var.update({'total_rating_count': var.get('total_rating_count',0) + int(score)  })

    # Increasing the number of voting sutdents by one
    var.update({'student_count': var.get('student_count',0) + 1  }) 

    # updating the course module
    store.update_item(course,request.user.id,custom_metadata=var)


    return HttpResponse('{"success":"true"}')





@csrf_exempt
def add_rate(request):
    '''
    Book and rating addition
    '''

    if request.method == 'GET':
        print 'GET Method .. \n'

        form = Thing_form()

        return render_to_response('ratings/thing_form.html', {'form':form})



    elif request.method == 'POST':
        print 'POST Method ..\n'

        thing = Thing()
        thing.book_name = request.POST['book_name']
        #thing.rating =request.POST['rating']

        thing.save()

        return HttpResponseRedirect('/2/')

    
        

    else:

        print 'Non Accepted Method .. \n'
