from django.views.decorators.csrf import ensure_csrf_cookie
from xmodule.modulestore.django import modulestore
from courseware.access import has_access
from util.json_request import JsonResponse
from courseware.grades import grade
from opaque_keys.edx import locator
from util.cache import cache_if_anonymous
from django.conf import settings
from courseware.courses import get_courses, sort_by_announcement
from edxmako.shortcuts import render_to_response
from django.contrib.auth.models import AnonymousUser
import logging
#from edraak_misc.utils import sort_closed_courses_to_bottom

log = logging.getLogger(__name__)


def all_courses(request):

    # The course selection work is done in courseware.courses.
    domain = settings.FEATURES.get('FORCE_UNIVERSITY_DOMAIN')  # normally False
    
    # do explicit check, because domain=None is valid
    if domain is False:
        domain = request.META.get('HTTP_HOST')



    if request.method =='POST':
        
        sort_value = request.POST.get('sort_value')

        if sort_value == '0':
            courses = get_courses(AnonymousUser(), domain=domain)
            context = {'courses': courses}
        elif sort_value == '1':
            courses = get_courses(AnonymousUser(), domain=domain)
            context = {'courses': courses.reverse()}
        elif sort_value == '2':
            context = {'courses': []}
        else:
            return JsonResponse({'error':'Option not allowed'})


        return render_to_response('all_courses.html', context)


    elif request.method == "GET":
    
        # Hardcoded `AnonymousUser()` to hide unpublished courses always
        courses = get_courses(AnonymousUser(), domain=domain)
        courses = sort_by_announcement(courses)
        #courses = sort_closed_courses_to_bottom(courses)


        context = {}
        ############ Ordering courses according to users selection ##################
        #############################################################################
        # Newest first
        if request.GET.get("sort_value") == '0':
            courses = courses
            context.update( {'zero':True} )


        # Oldest first    
        elif request.GET.get("sort_value") == '1':
            # becuase list.reverse() modifies list in place and makes the original one empty, we use the blow method
            courses = list(reversed(courses))
            context.update( {'one':True} )

        # Most Enrolled students courses
        elif request.GET.get("sort_value") == '2':

            from student.models import CourseEnrollment as CE

            # Enrolled-students/couse pairs
            pairs = [ ( CE.enrollment_counts(course.id) , course ) for course in courses]
            sorted_pairs = sorted(pairs,key=lambda x:x[0],reverse=True)

            courses = [course[1] for course in sorted_pairs]
            context.update( {'two':True} )

        # Least Enrolled students courses
        elif request.GET.get("sort_value") == '3':

            from student.models import CourseEnrollment as CE

            # Enrolled-students/couse pairs
            pairs = [ ( CE.enrollment_counts(course.id) , course ) for course in courses]
            sorted_pairs = sorted(pairs,key=lambda x:x[0])

            courses = [course[1] for course in sorted_pairs]
            context.update( {'three':True} )
        #############################################################################    


        context.update( {'courses': courses} )


        #context.update(extra_context)
        return render_to_response('all_courses.html', context)
