from django.conf.urls import patterns, url

urlpatterns = patterns('',
    url(r'^all_courses/$', 'zad_all_courses.views.all_courses', name='zad_all_courses'),
)
