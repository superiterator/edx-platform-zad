from django.conf.urls import patterns, url

urlpatterns = patterns('',
    url(r'^recommended_courses/$', 'zad_course_recommendation.views.recommend_courses', name='recommend_courses'),
)