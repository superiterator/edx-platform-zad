from pymongo import MongoClient
from datetime import datetime
import logging

from opaque_keys.edx.keys import CourseKey
from student.models import CourseEnrollment
from branding import modulestore


store = modulestore()

################################
connection = MongoClient("localhost",27017)

# edxapp MongoDB database
db = connection.edxapp
################################



log = logging.getLogger(__name__)


def create_update_course_category(course_id_string, category , rating=0):

	'''
		Creating a new entry or updating an existing one (recommendations collection in MongoDB edxapp DB)
		that relates/categorizes a course to a certain category
		rating: is not currently needed, but might be in the future in case we install some 
		recommender or Machine learning system that best recommends new courses to the students

		Expected format:
		{'category':'CATEGORY_A' , 'ORGX/course_name1/run_timea':['ORGX/course_name1/run_timea',CATEGORY_A, 0 , datetime.datetime(2015, 4, 23, 13, 2, 15, 641637)]}
		{'category':'CATEGORY_B' , 'ORGY/course_name2/run_timeb':['ORGY/course_name2/run_timeb',CATEGORY_B, 0 , datetime.datetime(2015, 5, 24, 13, 2, 15, 641637)]}
		{'category':'CATEGORY_C' , 'ORGZ/course_name3/run_timec':['ORGZ/course_name3/run_timec',CATEGORY_C, 0 , datetime.datetime(2015, 6, 25, 13, 2, 15, 641637)]}
		{'category':'CATEGORY_D' , 'ORGM/course_name4/run_timed':['ORGM/course_name4/run_timed',CATEGORY_D, 0 , datetime.datetime(2015, 7, 26, 13, 2, 15, 641637)]}
	'''
	
	
	course_data_update = { course_id_string : [course_id_string , category , rating, datetime.now()] }

	category_item = db.recommendations.find_one({'category':category})

	# If this category exists
	if category_item:  

	    category_item['data'].update(course_data_update)        
	    
	    # We can use flag to identify updates from new insertions, currently not needed
	    flag = db.recommendations.update({'category':category}, category_item ,upsert=True)
	    
	else:

	    flag = db.recommendations.update({'category':category}, {'category':category , 'data':course_data_update} ,upsert=True)

    



################################################################################################

# we do this in student area. Getting course enrollment for a certain student


def getting_latest_courses_with_related_categories(request):
	
	'''
		This function returns the latest 3 courses in each category the 
		student picks his courses in
	'''


	# Getting student courses enrollment
	user = request.user

	course_enrollment_list_for_user = CourseEnrollment.enrollments_for_user(user) 


	# set, a list of non-repeatable entries of categories
	categories_set = set()


	# [ categories_set.add( 
	# 					store.get_course(enrollment.course_id)._edit_info['category'] 
	# 					) 
	# 	for enrollment in course_enrollment_list_for_user ]


	for enrollment in course_enrollment_list_for_user:

		course_object = store.get_course(enrollment.course_id)

		if course_object:

			categories_set.add( course_object._edit_info.get("category","No Category") )


	# finding all courses in student's categories_set
	# categories_list -->  is a list of dicts, where each dict represents a category with a list of courses
	categories_list = db.recommendations.find({'category':{'$in':list(categories_set)}})


	recommended_courses_from_all_categories = []

	for category_dict in categories_list:
	    
	    list_of_courses = category_dict['data'].values()
	    date_ordered_list_of_courses = sorted( list_of_courses , key=lambda course: course[3]) 
	    
	    print date_ordered_list_of_courses
	        
	    # Taking only the last 3 courses in each category to offer to a student
	    recommended_courses_from_all_categories.append( date_ordered_list_of_courses[:3] )  

	    

	# Flattening all the encapsulated lists in one flat list
	#all_courses_flattened = [ course for category in recommended_courses_from_all_categories for course in category ]
	all_course_names_flattened = [ course[0] for category in recommended_courses_from_all_categories for course in category ]

	return all_course_names_flattened


