from edxmako.shortcuts import render_to_response

from opaque_keys.edx.keys import CourseKey
from branding import modulestore


from .course_recommendations import create_update_course_category , \
									getting_latest_courses_with_related_categories



store = modulestore()


def recommend_courses(request):

	'''
		1- Getting all the courses IDs the student is enrolled in
		2- Converting the courses IDs to CourseKey objects (Locators)
		3- Retreiving all courses objects from MongoDB
		4- Extracting student's courses objects by matching the student courses Locators to courses retreived from MongoDB
		5- We do the above step by using sets, and find the intersections between the 2 sets
	'''

	# courses that the user has enrolled in
	student_returned_courses = getting_latest_courses_with_related_categories(request)

	# CourseKey objects for the returned courses
	student_courses_keys = [ CourseKey.from_string(course) for course in student_returned_courses ]

	

	# Grabbing all the courses objects from mongoDB
	all_courses = store.get_courses()

	# Making a course location versus course object hash table
	courses_dict = {}

	[ courses_dict.update( { course.id : course } ) for course in all_courses ]


	all_courses_keys = courses_dict.keys()


	# Finding courses intersections
	keys_intersection = set(student_courses_keys) & set(all_courses_keys)


	courses_to_return = []
	
	for course_key in list(keys_intersection):
		courses_to_return.append( courses_dict.get(course_key) )


	context = {
		'courses' : courses_to_return,
	}	


	return render_to_response('recommended_courses.html', context)

