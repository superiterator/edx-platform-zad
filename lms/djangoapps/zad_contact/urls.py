from django.conf import settings
from django.conf.urls import patterns, url
from django.core.urlresolvers import LocaleRegexURLResolver

urlpatterns = patterns('',
        url(r'', 'zad_contact.views.contact', name="contact"),
)

